#!/bin/sh
# 
# 2022      Sebastian Mogl (firefox at treuchtlingen.de)
# 
# This file is part of our browser for governmental use.
#
# makethemes.sh is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# makethemes.sh is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with cinit. If not, see <http://www.gnu.org/licenses/>.
#
#
# makethemes.sh will gegerate the theme xpi files from source files 
# inside this project.

make_manifest() {
cat > "$THEME_DIR/manifest.json" <<EOF
{
  "name": "$THEME_NAME",
  "manifest_version": 2,
  "version": "6.2",
  "theme": {
    "images": {
      "theme_frame": "background.svg"
    },
    "colors": {
      "icons_attention": "$THEME_ACCENT_1",
      "toolbar_field_border_focus": "$THEME_ACCENT_2",
      "frame": "$THEME_BACKGROUND",
      "tab_background_text": "$THEME_HIGHLIGHT_TEXT",
      "toolbar": "$THEME_SECONDARY_BACKGROUND",
      "toolbar_field": "$THEME_TERTIARY_BACKGROUND",
      "toolbar_field_text": "$THEME_TEXT",
      "toolbar_field_text_focus": "$THEME_HIGHLIGHT_TEXT",
      "tab_line": "$THEME_ACCENT_1"
    }
  },
  "browser_specific_settings": {
		"gecko": {
		  "id": "$THEME_ID",
		  "strict_min_version": "91.0"
		}
	}
}
EOF
}

make_theme() {
	echo "Generating Theme $THEME_NAME [$THEME_ID] ..."
	THEME_ID="$1"
	THEME_NAME="$2"
	THEME_DIR="$THEME_BASE_DIR/$THEME_ID"
	mkdir -p "$THEME_DIR"
	cp "backgrounds/$THEME_BACKGROUND_IMAGE" "$THEME_DIR/background.svg"
	make_manifest
	echo "Packing Things up ..."
	OLD_PWD="$PWD"
	cd "$THEME_DIR"
	mkdir -p "$OUTDIR_ABSOLUTE"
	zip -r "$OUTDIR_ABSOLUTE/$THEME_ID.xpi" *
	cd "$OLD_PWD"
	echo "Done!"
}

FOREST_GREEN="#004b4b"
FRESH_GREEN="#559b73"
GOLDEN_BROWN="#b4822d"
WARM_LIGHT_GREY="#dcd7cd"

THEME_ACCENT_1="$GOLDEN_BROWN"
THEME_ACCENT_2="$FRESH_GREEN"

THEME_BASE_DIR="themes"
OUTDIR_ABSOLUTE="$PWD/xpi"

# Make dark theme

THEME_BACKGROUND="#222222"
THEME_SECONDARY_BACKGROUND="#00000088"
THEME_TERTIARY_BACKGROUND="#333333"

THEME_HIGHLIGHT_TEXT="#ffffff"
THEME_TEXT="#cccccc"

THEME_BACKGROUND_IMAGE=dark_foxears.svg
make_theme "tr-dark@treuchtlingen.de" "Treuchtlingen Dunkel"

THEME_BACKGROUND_IMAGE=dark_plain.svg
make_theme "tr-dark-plain@treuchtlingen.de" "Treuchtlingen Dunkel (Ohne Fuchs)"

# Make Green Dark theme

THEME_BACKGROUND="$FOREST_GREEN"
THEME_SECONDARY_BACKGROUND="${FRESH_GREEN}44"

THEME_BACKGROUND_IMAGE=green-dark_foxears.svg
make_theme "tr-green-dark@treuchtlingen.de" "Treuchtlingen Waldgrün"

THEME_BACKGROUND_IMAGE=green-dark_plain.svg
make_theme "tr-green-dark-plain@treuchtlingen.de" "Treuchtlingen Waldgrün (Ohne Fuchs)"

# Make Light theme

THEME_BACKGROUND="$WARM_LIGHT_GREY"
THEME_SECONDARY_BACKGROUND="#ffffff88"
THEME_TERTIARY_BACKGROUND="$WARM_LIGHT_GREY"

THEME_HIGHLIGHT_TEXT="#000000"
THEME_TEXT="#343a40"

THEME_BACKGROUND_IMAGE=light_foxears.svg
make_theme "tr-light@treuchtlingen.de" "Treuchtlingen Hell"

THEME_BACKGROUND_IMAGE=light_plain.svg
make_theme "tr-light-plain@treuchtlingen.de" "Treuchtlingen Hell (Ohne Fuchs)"

# Make Light Green Theme

THEME_BACKGROUND="$FRESH_GREEN"
THEME_SECONDARY_BACKGROUND="#ffffff44"

THEME_BACKGROUND_IMAGE=green-light_foxears.svg
make_theme "tr-green-light@treuchtlingen.de" "Treuchtlingen Frisches Grün"

THEME_BACKGROUND_IMAGE=green-light_plain.svg
make_theme "tr-green-light-plain@treuchtlingen.de" "Treuchtlingen Frisches Grün (Ohne Fuchs)"


